#ifndef MODEL_FORMAT_H
#define MODEL_FORMAT_H

struct model_header
{
    char Format[4]; // M O D L ?
    char Version[4];
};

struct index_buffer_info
{
    long Target; // If it's GL_UNSIGNED_SHORT etc.
    int IndexCount;
    long ByteLength;
};

struct animation_vec_channel
{
    double TimeStamp;
    float Vector[3];
};

struct animation_quat_channel
{
    double TimeStamp;
    float Quaternion[4];
};

struct animation_header
{
    int AnimationCount;
};

struct joint_header
{
    int JointCount;
};

struct joint
{
    float Translation[3];
    float Rotation[4];
    float Scale[3];
    float InverseBindMatrix[4][4];
    
    unsigned int ChildCount;
    unsigned int Children[20];
};

struct mesh_header
{
    long NumVertices;
    long VertexChunkSize;
    long NumNormals;
    long NumUVs;
    long NormalsChunkSize;
    long NumFaces;
    long FacesChunkSize;
    bool HasTexture;
    char TextureFile[100];
};


struct vertex_attribute
{
    long Type;
    long Stride;
    long Offset;
};

struct mesh_data_info
{
    long IndexBufferTarget; // If it's GL_UNSIGNED_SHORT etc.
    int IndexCount;
    long IndexBufferByteLength;
    long VertexBufferByteLength;
    bool HasSkin;
    //int VertexAttributeCount;
    
    // @Incomplete: Skinning and animation
    
    //b32 HasTexture; // @Incomplete: Load these at the same time as the buffers
    //char TextureName[100];
};

struct mesh_data
{
    mesh_data_info Info;
    char* IndexBuffer;
    char* VertexBuffer;
    //vertex_attribute* VertexAttributes;
};

struct chunk_format
{
    char Format[4]; // M E S H / E O F / B O N E / A N I M
};

#endif
