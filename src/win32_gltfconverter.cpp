#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION

#include "tiny_gltf.h"
#include "modelformat.h"
#include <iostream>"

using namespace tinygltf;

static void CopyBuffer(char* Destination, const tinygltf::Buffer Source, long byteLength)
{
}

int main(int Argc, char** Argv)
{
    Model Model;
    TinyGLTF Loader;
    std::string Err;
    
    //bool ret = loader.LoadASCIIFromFile(&model, &err, "../files/cube.gltf");//argv[1]);
	//bool Ret = Loader.LoadBinaryFromFile(&Model, &Err, "../files/project_polly.glb");//argv[1]); // for binary glTF(.glb)
    //bool Ret = Loader.LoadASCIIFromFile(&Model, &Err, "../files/polly/project_polly.gltf"); //argv[1]); // for binary glTF(.glb) 
    bool Ret = Loader.LoadBinaryFromFile(&Model, &Err, "../files/wolf.glb");
    if (!Err.empty()) {
        printf("Err: %s\n", Err.c_str());
    }
    
    if (!Ret) {
        printf("Failed to parse glTF\n");
        return -1;
    }
    
    // Let's write this into our own file format
    std::ofstream OFS("../files/exported/test.modl", std::ofstream::binary);
    
    // Write the main header
    model_header ModelHeader;
    sprintf(ModelHeader.Format, "MODL");
    sprintf(ModelHeader.Version, "1.4");
    
    OFS.write(reinterpret_cast<char *>(&ModelHeader), sizeof(model_header));
    
    int IndexStartOfSkins = 1000;
    int IndexEndOfSkins = 0;
    int JointCount = 0;
    
    for (int Index = 0; Index < Model.nodes.size(); Index++)
    {
        if (Model.nodes[Index].skin >= 0)
        {
            auto Skin = Model.skins[Model.nodes[Index].skin];
            for (int JointIndex = 0; JointIndex < Skin.joints.size(); JointIndex++)
            {
                if (Skin.joints[JointIndex] < IndexStartOfSkins)
                    IndexStartOfSkins = Skin.joints[JointIndex];
                if (Skin.joints[JointIndex] > IndexEndOfSkins)
                    IndexEndOfSkins = Skin.joints[JointIndex];
            }
        }
    }
    
    JointCount = IndexEndOfSkins - IndexStartOfSkins + 1;
    
    joint* Joints = (joint*)malloc(JointCount * sizeof(joint));
    
    int I = 0;
    for (int JointIndex = IndexStartOfSkins; JointIndex <= IndexEndOfSkins; JointIndex++)
    {
        auto Node = Model.nodes[JointIndex];
        auto& Joint = Joints[I];
        Joint.ChildCount = Node.children.size();
        
        for (int ChildIndex = 0; ChildIndex < Node.children.size(); ChildIndex++)
        {
            Joint.Children[ChildIndex] = Node.children[ChildIndex] - IndexStartOfSkins;
        }
        
        if (Node.translation.size() > 0)
        {
            Joint.Translation[0] = Node.translation[0];
            Joint.Translation[1] = Node.translation[1];
            Joint.Translation[2] = Node.translation[2];
        }
        else
        {
            Joint.Translation[0] = 0;
            Joint.Translation[1] = 0;
            Joint.Translation[2] = 0;
        }
        
        if (Node.rotation.size() > 0)
        {
            Joint.Rotation[0] = Node.rotation[0];
            Joint.Rotation[1] = Node.rotation[1];
            Joint.Rotation[2] = Node.rotation[2];
            Joint.Rotation[3] = Node.rotation[3];
        }
        else
        {
            Joint.Rotation[0] = 0;
            Joint.Rotation[1] = 0;
            Joint.Rotation[2] = 0;
            Joint.Rotation[3] = 0;
        }
        
        if (Node.scale.size() > 0)
        {
            Joint.Scale[0] = Node.scale[0];
            Joint.Scale[1] = Node.scale[1];
            Joint.Scale[2] = Node.scale[2];
        }
        else
        {
            Joint.Scale[0] = 0;
            Joint.Scale[1] = 0;
            Joint.Scale[2] = 0;
        }
        
        I++;
    }
    
    // Write the mesh data
    for (int Index = 0; Index < Model.nodes.size(); Index++)
    {
        auto Node = Model.nodes[Index];
        
        if (Node.mesh != -1)
        {
            auto Mesh = Model.meshes[Node.mesh];
            
            for (int PrimitiveIndex = 0; PrimitiveIndex < Mesh.primitives.size(); PrimitiveIndex++)
            {
                const tinygltf::Primitive &Primitive = Mesh.primitives[PrimitiveIndex]; // @Incomplete: We need to support multiple primitives
                Skin Skin;
                
                if (Node.skin >= 0)
                    Skin = Model.skins[Node.skin];
                
                chunk_format Format;
                sprintf(Format.Format, "MESH");
                OFS.write(reinterpret_cast<char *>(&Format), sizeof(chunk_format));
                
                mesh_data_info MeshDataInfo;
                int VertexCount;
                unsigned short* IndexBuffer;
                float* VertexBuffer = 0;
                float* NormalBuffer = 0;
                float* TexcoordBuffer = 0;
                unsigned short* JointBuffer = 0;
                float* WeightBuffer = 0;
                
                vertex_attribute* VertexAttributes;
                
                auto IndicesAccessor = Model.accessors[Primitive.indices];
                auto IndexBufferView = Model.bufferViews[IndicesAccessor.bufferView];
                
                MeshDataInfo.IndexBufferTarget = IndexBufferView.target;
                MeshDataInfo.IndexBufferByteLength = IndicesAccessor.count * sizeof(unsigned short);
                MeshDataInfo.IndexCount = IndicesAccessor.count; // Not sure that this is the right count, but let's see what happens
                
                auto Buffer = Model.buffers[IndexBufferView.buffer];
                IndexBuffer = (unsigned short*)malloc(MeshDataInfo.IndexBufferByteLength);
                
                memcpy(IndexBuffer, Buffer.data.data() + IndexBufferView.byteOffset, MeshDataInfo.IndexBufferByteLength);
                
                for (const std::pair<const std::string, int>& Attribute : Primitive.attributes)
                {
                    const tinygltf::Accessor &Accessor = Model.accessors[Attribute.second];
                    
                    auto attributeName = Attribute.first;
                    if (attributeName.compare("POSITION") == 0)
                    {
                        auto BufferView = Model.bufferViews[Accessor.bufferView];
                        
                        //MeshDataInfo.VertexBufferTarget = BufferView.target;
                        MeshDataInfo.VertexBufferByteLength = BufferView.byteLength;
                        VertexCount = Accessor.count;
                        
                        const tinygltf::Buffer &VertexArray = Model.buffers[BufferView.buffer];
                        
                        VertexBuffer = (float*)malloc(BufferView.byteLength);
                        memcpy(VertexBuffer, VertexArray.data.data() + BufferView.byteOffset, BufferView.byteLength);
                    }
                    else if (attributeName.compare("NORMAL") == 0)
                    {
                        auto BufferView = Model.bufferViews[Accessor.bufferView];
                        
                        const tinygltf::Buffer &VertexArray = Model.buffers[BufferView.buffer];
                        MeshDataInfo.VertexBufferByteLength += BufferView.byteLength;
                        
                        NormalBuffer = (float*)malloc(BufferView.byteLength);
                        memcpy(NormalBuffer, VertexArray.data.data() + BufferView.byteOffset, BufferView.byteLength);
                    }
                    else if (attributeName.compare("TANGENT") == 0)
                    {
                    }
                    else if (attributeName.compare("TEXCOORD_0") == 0)
                    {
                        auto BufferView = Model.bufferViews[Accessor.bufferView];
                        MeshDataInfo.VertexBufferByteLength += BufferView.byteLength;
                        
                        const tinygltf::Buffer &VertexArray = Model.buffers[BufferView.buffer];
                        
                        TexcoordBuffer = (float*)malloc(BufferView.byteLength);
                        memcpy(TexcoordBuffer, VertexArray.data.data() + BufferView.byteOffset, BufferView.byteLength);
                    }
                    else if (attributeName.compare("JOINTS_0") == 0)
                    {
                        auto BufferView = Model.bufferViews[Accessor.bufferView];
                        MeshDataInfo.VertexBufferByteLength += BufferView.byteLength;
                        
                        const tinygltf::Buffer &VertexArray = Model.buffers[BufferView.buffer];
                        
                        JointBuffer = (unsigned short*)malloc(BufferView.byteLength);
                        memcpy(JointBuffer, VertexArray.data.data() + BufferView.byteOffset, BufferView.byteLength);
                    }
                    else if (attributeName.compare("WEIGHTS_0") == 0)
                    {
                        auto BufferView = Model.bufferViews[Accessor.bufferView];
                        MeshDataInfo.VertexBufferByteLength += BufferView.byteLength;
                        
                        const tinygltf::Buffer &VertexArray = Model.buffers[BufferView.buffer];
                        
                        WeightBuffer = (float*)malloc(BufferView.byteLength);
                        memcpy(WeightBuffer, VertexArray.data.data() + BufferView.byteOffset, BufferView.byteLength);
                    }
                }
                
                std::vector<float> data;
                
                // Pack vertex buffer
                for (int Index = 0; Index < VertexCount; Index++)
                {
                    data.push_back(VertexBuffer[Index * 3]);
                    data.push_back(VertexBuffer[Index * 3 + 1]);
                    data.push_back(VertexBuffer[Index * 3 + 2]);
                    
                    if (NormalBuffer)
                    {
                        data.push_back(NormalBuffer[Index * 3]);
                        data.push_back(NormalBuffer[Index * 3 + 1]);
                        data.push_back(NormalBuffer[Index * 3 + 2]);
                    }
                    else
                    {
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                    }
                    
                    if (TexcoordBuffer)
                    {
                        data.push_back(TexcoordBuffer[Index * 2]);
                        data.push_back(TexcoordBuffer[Index * 2 + 1]);
                    }
                    else
                    {
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                    }
                    
                    if (JointBuffer && Skin.joints.capacity() > 0)
                    {
                        unsigned int JointIndex1 = Skin.joints[JointBuffer[Index * 4]] - IndexStartOfSkins;
                        unsigned int JointIndex2 = Skin.joints[JointBuffer[Index * 4 + 1]] - IndexStartOfSkins;
                        unsigned int JointIndex3 = Skin.joints[JointBuffer[Index * 4 + 2]] - IndexStartOfSkins;
                        unsigned int JointIndex4 = Skin.joints[JointBuffer[Index * 4 + 3]] - IndexStartOfSkins;
                        
                        data.push_back((float)JointIndex1);
                        data.push_back((float)JointIndex2);
                        data.push_back((float)JointIndex3);
                        data.push_back((float)JointIndex4);
                        /*
                        data.push_back((float)JointBuffer[Index * 4]);
                        data.push_back((float)JointBuffer[Index * 4 + 1]);
                        data.push_back((float)JointBuffer[Index * 4 + 2]);
                        data.push_back((float)JointBuffer[Index * 4 + 3]);
                        */
                        data.push_back(WeightBuffer[Index * 4]);
                        data.push_back(WeightBuffer[Index * 4 + 1]);
                        data.push_back(WeightBuffer[Index * 4 + 2]);
                        data.push_back(WeightBuffer[Index * 4 + 3]);
                    }
                    else
                    {
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                        
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                        data.push_back(0.0f);
                    }
                }
                
                // Write mesh_data_info
                // Write index buffer
                // Write vertex buffers
                // @Incomplete: Write vertex_attributes
                
                MeshDataInfo.HasSkin = false;
                
                MeshDataInfo.VertexBufferByteLength = data.capacity() * sizeof(float);
                OFS.write(reinterpret_cast<const char*>(&MeshDataInfo), sizeof(mesh_data_info));
                OFS.write(reinterpret_cast<const char*>(IndexBuffer), MeshDataInfo.IndexBufferByteLength);
                OFS.write(reinterpret_cast<const char*>(&data.data()[0]), data.capacity() * sizeof(float));
                
                free(IndexBuffer);
                free(VertexBuffer);
                
                if (NormalBuffer)
                    free(NormalBuffer);
                
                if (TexcoordBuffer)
                    free(TexcoordBuffer);
                
                if (JointBuffer)
                    free(JointBuffer);
                
                if (WeightBuffer)
                    free(WeightBuffer);
            }
        }
    }
    
    chunk_format Format;
    sprintf(Format.Format, "SKIN");
    joint_header JointHeader;
    JointHeader.JointCount = JointCount;
    OFS.write(reinterpret_cast<const char*>(&Format), sizeof(chunk_format));
    OFS.write(reinterpret_cast<const char*>(&JointHeader), sizeof(joint_header));
    OFS.write(reinterpret_cast<const char*>(&Joints[0]), sizeof(joint) * JointCount);
    
    sprintf(Format.Format, "ANIM");
    animation_header AnimationHeader;
    AnimationHeader.AnimationCount = Model.animations.size();
    
    for (int AnimationIndex = 0; AnimationIndex < Model.animations.size(); AnimationIndex++)
    {
        auto Animation = Model.animations[AnimationIndex];
        std::vector<animation_vec_channel> Tranlations;
        std::vector<animation_quat_channel> Rotations;
        std::vector<animation_vec_channel> Scales;
        
        for(int ChannelIndex = 0; ChannelIndex < Animation.channels.size(); ChannelIndex++)
        {
            auto Channel = Animation.channels[ChannelIndex];
            auto Sampler = Animation.samplers[Channel.sampler];
            auto BufferView = Model.bufferViews[Model.accessors[Sampler.input].bufferView];
            double Time = 0.0f;
            Time = (double)*(((char*)Model.buffers[BufferView.buffer].data.data()) + BufferView.byteOffset);
			auto ddamdmad = (char*)Model.buffers[BufferView.buffer].data.data() + BufferView.byteOffset;
		}
    }
    
    sprintf(Format.Format, "EOF");
    OFS.write(reinterpret_cast<const char*>(&Format), sizeof(chunk_format));
    OFS.close();
    
    return 0;
}